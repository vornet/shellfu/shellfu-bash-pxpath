%global sfincb %{_datadir}/shellfu/include-bash
%global sfmodn __SHELLFU_MODNAME__
%global shellfu_req shellfu >= __VDEP_SHELLFU_GE__, shellfu < __VDEP_SHELLFU_LT__
%if ( 0%{?rhel} && 0%{?rhel} > 7 ) || ( 0%{?centos} && 0%{?centos} > 7 ) || ( 0%{?fedora} && 0%{?fedora} > 31 )
%global py3 1
%global lxml_req python3-libxml2
%else
%global py3 0
%global lxml_req libxml2-python
%endif

Name:       __MKIT_PROJ_PKGNAME__
Version:    __MKIT_PROJ_VERSION__
Release:    1%{?dist}
Summary:    __MKIT_PROJ_NAME__ - __MKIT_PROJ_TAGLINE__
URL:        __MKIT_PROJ_VCS_BROWSER__
License:    LGPLv2
Source0:    %{name}-%{version}.tar.gz
BuildArch:  noarch
BuildRequires: %lxml_req
BuildRequires: %shellfu_req
BuildRequires: make
BuildRequires: shellfu-bash-pretty
%if 0%{?fedora} == 26
BuildRequires: /usr/bin/python
%endif

Requires: %lxml_req
Requires: %shellfu_req
Requires: shellfu-bash
Requires: shellfu-bash-pretty
%description
__SHELLFU_MODDESC__

%prep
%setup -q

%build
make %{?_smp_mflags} PREFIX=/usr

%install
%make_install PREFIX=/usr
%if %{py3}
    sed -i '1s/python2\>/python3/' \
        "%{buildroot}/%{_libexecdir}/shellfu-bash-pxpath/pxpath"
%endif

%check
make test \
    SHELLFU_PATH=%{buildroot}/%{_datadir}/shellfu/include-bash \
    _PXPATH__BIN=%{buildroot}/%{_libexecdir}/shellfu-bash-pxpath/pxpath

%files
%dir %{_libexecdir}/shellfu-bash-pxpath
%doc %{_docdir}/%{name}/README.md
%{sfincb}/%{sfmodn}.sh
%{_libexecdir}/shellfu-bash-pxpath/pxpath

%changelog

# specfile built with MKit __MKIT_MKIT_VERSION__
