pxpath
======

pxpath is [Shellfu/Bash][sf] wrapper around libxml2-python with intent
to enable you to make simple XPath queries from a shell script,
while retaining compatibility with ancient distributions such as
RHEL-5.

  [sf]: https://gitlab.com/vornet/shellfu/shellfu-bash-pxpath
